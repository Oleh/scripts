@echo off
call :treeProcess
goto :eof

:treeProcess

for /D %%d in (*) do (
	cd %%d
	if exist .git ( 
		echo ---------------------------------------------
		echo 		%%d
		echo ---------------------------------------------
		git pull
		cd ..
	) else (
		call :treeProcess
		cd ..
	)
)
exit /b